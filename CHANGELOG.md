# Changelog

# [0.11.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.10.0...v0.11.0) (2022-02-16)


### :bug:

* Fix presistent storage ([64318b4](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/64318b4aa170ceacd1972532358aee98fff52a8b))

# [0.10.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.9.0...v0.10.0) (2022-02-16)


### :bug:

* Fix presistent storage ([2ffbf90](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/2ffbf90a79f470a368e6861ed284674026732f2c))

# [0.9.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.8.0...v0.9.0) (2022-02-16)


### :bug:

* Fix ingress ([3108e94](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/3108e947c570e3bf43e9f882b4b6797751054fab))

# [0.8.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.7.0...v0.8.0) (2022-02-15)


### :bug:

* Fix ingress ([1b4b853](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/1b4b853ec2ca45ceaf3ae8accf64fa91d546b4b6))

# [0.7.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.6.0...v0.7.0) (2022-02-15)


### :bug:

* Fix ingress ([9b2383e](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/9b2383e22b3b805a6346d8a1626d049bad0842c6))

# [0.6.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.5.0...v0.6.0) (2022-02-15)


### :bug:

* Fix ingress ([db238b5](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/db238b515aa212ef568cba3e72895fbb6c68c706))

# [0.5.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.4.0...v0.5.0) (2022-02-15)


### :bug:

* Fix ingress ([41bfb55](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/41bfb5534b811090a01b753cbeff5a7253cc2919))

# [0.4.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.3.0...v0.4.0) (2022-02-15)


### :bug:

* Fix ingress ([68b75fc](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/68b75fc1d853d265a9b8d474fec344b37366541d))

# [0.3.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.2.0...v0.3.0) (2022-02-15)


### :bug:

* Fix ingress ([3a03e9e](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/3a03e9ed8e502c90ad3d9b8d104fdfea00cd2ad7))

# [0.2.0](https://gitlab.com/programmer-assistant/addon-pi-hole/compare/v0.1.3...v0.2.0) (2022-02-15)


### :bug:

* Fix ingress ([bf09388](https://gitlab.com/programmer-assistant/addon-pi-hole/commit/bf0938801ea83c93424ee6d8bbccd1221ff7038b))

## 0.1.0

- Initial build based on pi-hole 2022.02.1
